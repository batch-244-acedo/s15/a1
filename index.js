let firstName = "James";
console.log("First Name: " + firstName);

let lastName = "Acedo";
console.log("Last Name: " + lastName);

let age1 = "30";
console.log("Age: " + age1);

let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
console.log("Hobbies: ");
console.log(hobbies);

let workAddress = {
	houseNumber: 32,
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska",
}
console.log("Work Address: ");
console.log(workAddress);


let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce", "Thor", "Natasha","Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullName1 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName1);

	const lastLocation = "Arctic Ocean";
	// lastLocation = "Atlantic Ocean"; - lastLocation is decalred as constant
	console.log("I was found frozen in: " + lastLocation);
